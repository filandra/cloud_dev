﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Vision.V1;
using System;
using Grpc.Auth;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using Google.Cloud.Storage.V1;

namespace NSWI152_Camera.Classes
{
    class Program
    {
        static string imagesDirectory = "Data/Images/";

        static ImageAnnotatorClient annotatorClient = null;        
        static string filePath = "";
        static List<string> wantedLabes = new List<string>();

        static string projectId = "nswi152-camera";
        static StorageClient storageClient = null;

        static void Main(string[] args)
        {
            Init();            
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("[1] capture");
                Console.WriteLine("[2] verbose capture");
                Console.WriteLine("[3] download");
                Console.WriteLine("[4] delete");
                Console.WriteLine("[5] close");
                string command = Console.ReadLine();
                string date = "";
                switch (command)
                {
                    case "1":
                        capture();
                        break;
                    case "2":
                        capture(true);
                        break;
                    case "3":
                        Console.WriteLine("Write the date in format day_month_year");
                        date = Console.ReadLine();
                        download(date);
                        break;
                    case "4":
                        Console.WriteLine("WARNING - DELETING");
                        Console.WriteLine("Write the date in format day_month_year");
                        date = Console.ReadLine();
                        deleteBucket(date);
                        break;
                    case "5":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Wrong input");
                        break;
                }
            }
        }

        private static void Init()
        {
            // Message for users
            Console.WriteLine("-------------");
            Console.WriteLine("CloudCam-1984");
            Console.WriteLine("-------------");

            // Load credentials and create clients
            // You will need to add your own credentials to the file
            var credential = GoogleCredential.FromFile("Data/NSWI152-Camera-credentials.json");
            var channel = new Grpc.Core.Channel(
                ImageAnnotatorClient.DefaultEndpoint.ToString(),
                credential.ToChannelCredentials());

            annotatorClient = ImageAnnotatorClient.Create(channel);          
            storageClient = StorageClient.Create(credential);

            // Load wanted labels from config
            using (var reader = new StreamReader("Data/config.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!string.IsNullOrEmpty(line))
                    {
                        wantedLabes.Add(line);
                    }
                }
            }

        }

        private static void capture(bool verbose = false)
        {
            Console.WriteLine("Press Escape to stop");
            do
            {
                 while (!Console.KeyAvailable)
                {
                    DateTime now = DateTime.Now;
                    string time = now.Hour + "_" + now.Minute + "_" + now.Second;
                    filePath = imagesDirectory + time + ".png";

                    CameraCapture.CaptureAndSaveImage(filePath);
                    CropAndSaveImage();

                    var image = Google.Cloud.Vision.V1.Image.FromFile(filePath);
                    var response = annotatorClient.DetectLabels(image);

                    // Save image if it contains wanted label
                    foreach (var label in response)
                    {                    
                        if (wantedLabes.Contains(label.Description))
                        {
                            Console.WriteLine("Detected target");
                            upload();
                            break;
                        }                    
                    }

                    //If verbose, write all labels and don't delete the image
                    // else just delete the image
                    if (verbose==true)
                    {
                        foreach (var label in response)
                        {
                            Console.WriteLine(label.Description);
                        }
                    }
                    else
                    {
                        FileInfo oldImage = new FileInfo(filePath);
                        oldImage.Delete();
                    }

                    Console.WriteLine();

                    Thread.Sleep(1000);
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }

        private static bool BucketExists(string bucketId)
        {
            var buckets = storageClient.ListBuckets(projectId);
            // Check if bucket exists
            foreach (var bucket in buckets)
            {
                if (bucket.Name == bucketId)
                {
                    return true;
                }
            }
            return false;
        }

        private static void upload()
        {
            DateTime now = DateTime.Now;
            string bucketId = now.Date.Day.ToString() + "_" + now.Date.Month.ToString() + "_" + now.Date.Year.ToString();
            bool bucketExists = BucketExists(bucketId);

            if (bucketExists==false)
            {
                Console.WriteLine("Creating new bucket for this day");
                storageClient.CreateBucket(projectId, bucketId);
                Console.WriteLine("Creation complete");
            }

            using (var f = File.OpenRead(filePath))
            {
                string imageName = Path.GetFileName(filePath);
                storageClient.UploadObject(bucketId, imageName, null, f);
                Console.WriteLine($"Uploaded {imageName}.");
            }
        }

        private static void download(string date)
        {
            bool bucketExists = BucketExists(date);           

            if (bucketExists==false)
            {
                Console.WriteLine("No data from this date");
                return;
            }

            foreach (var storageObject in storageClient.ListObjects(date, ""))
            {           
                string localPath = "Data/Downloads/" + date + "/" + storageObject.Name;
                //Create the directory, if missing
                System.IO.FileInfo file = new System.IO.FileInfo(localPath);
                file.Directory.Create();

                using (var outputFile = File.OpenWrite(localPath))
                {
                    storageClient.DownloadObject(date, storageObject.Name, outputFile);
                }
            }
        }

        private static void deleteBucket(string date)
        {
            bool bucketExists = BucketExists(date);
            if (bucketExists == false)
            {
                Console.WriteLine("Nothing to delete from this day");
                return;
            }

            List<string> images = new List<string>();
            foreach (var storageObject in storageClient.ListObjects(date, ""))
            {
                images.Add(storageObject.Name);
            }

            foreach (var image in images)
            {
                storageClient.DeleteObject(date, image);
            }
            storageClient.DeleteBucket(date);
            Console.WriteLine($"Deleted {date}.");
        }

        private static void CropAndSaveImage()
        {

            //Crop the wintasoft banner out
            var image = System.Drawing.Image.FromFile(filePath);
            var croppedImage = CropImage(image, new Rectangle(new Point(0, 40), new Size(image.Width, image.Height - 40)));
            image.Dispose();

            FileInfo oldImage = new FileInfo(filePath);
            oldImage.Delete();

            croppedImage.Save(filePath);
        }

        private static System.Drawing.Image CropImage(System.Drawing.Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        }
    }
}
