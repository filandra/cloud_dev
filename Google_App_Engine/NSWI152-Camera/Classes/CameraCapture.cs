﻿using System.Threading;

namespace NSWI152_Camera.Classes
{
    // Copied from https://www.vintasoft.com/docs/vsimaging-dotnet/Vintasoft.Imaging.Media/Vintasoft.Imaging.Media.DirectShowCamera.html
    public class CameraCapture
    {
        /// <summary>
        /// Indicates that image is captured from device.
        /// </summary>
        static bool _captureCompleted;

        static string filePath = "";

        /// <summary>
        /// Captures image from camera and saves captured image to a file.
        /// </summary>
        public static void CaptureAndSaveImage(string path)
        {
            filePath = path;
            _captureCompleted = false;

            // get a list of available cameras
            System.Collections.ObjectModel.ReadOnlyCollection<Vintasoft.Imaging.Media.ImageCaptureDevice> captureDevices =
                Vintasoft.Imaging.Media.ImageCaptureDeviceConfiguration.GetCaptureDevices();

            // if cameras are not found
            if (captureDevices.Count == 0)
            {
                System.Console.WriteLine("No connected devices.");
                return;
            }

            // get the first available camera
            Vintasoft.Imaging.Media.DirectShowCamera captureDevice =
                (Vintasoft.Imaging.Media.DirectShowCamera)captureDevices[0];

            // create the image capture source
            Vintasoft.Imaging.Media.ImageCaptureSource captureSource =
                new Vintasoft.Imaging.Media.ImageCaptureSource();
            // set the camera as a capture device of the image capture source
            captureSource.CaptureDevice = captureDevice;
            // subscribe to the image cptured event
            captureSource.CaptureCompleted +=
                new System.EventHandler<Vintasoft.Imaging.Media.ImageCaptureCompletedEventArgs>(captureSource_CaptureCompleted);

            // start the image capturing from the camera
            captureSource.Start();

            // asynchronously capture an image from camera
            captureSource.CaptureAsync();

            // while image is not captured
            while (!_captureCompleted)
                // wait
                Thread.Sleep(50);

            // stop the image capturing from the camera
            captureSource.Stop();
        }

        /// <summary>
        /// Image is captured from camera.
        /// </summary>
        private static void captureSource_CaptureCompleted(object sender, Vintasoft.Imaging.Media.ImageCaptureCompletedEventArgs e)
        {
            // get captured image as VintasoftImage object
            using (Vintasoft.Imaging.VintasoftImage capturedImage = e.GetCapturedImage())
            {
                // output information about captured image
                System.Console.WriteLine(string.Format("Image captured", capturedImage.Width, capturedImage.Height));

                // save captured image to a file
                capturedImage.Save(filePath);
            }

            // indicate that image is captured
            _captureCompleted = true;
        }
    }
}
